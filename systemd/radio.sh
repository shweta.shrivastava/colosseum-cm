#!/bin/bash

echo "[`date`] Radio Starting"

export PYTHONPATH=/usr/local/lib/python3/dist-packages:/usr/local/lib/python3/site-packages

/usr/local/bin/radiod.py
/usr/local/bin/radio_wait.py off 30
/usr/local/bin/radio_boot.py

my_ip=`ip -o -4 addr list dev tr0 | awk '{print $4}' | cut -d/ -f1 | cut -d. -f1-3`
for i in {1..254}; do
    ping -c 1 $my_ip.$i &
done
wait

echo "[`date`] Radio Started"
