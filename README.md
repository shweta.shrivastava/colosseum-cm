# Colosseum CM

Set of ansible playbooks that build srsLTE images for Colosseum.

## Provision

To install all the requirements (ansible, lxc, etc.), run

```
./provision.sh
```

Fill in team and user name information when prompted.  The script may take a
while to finish.

Once finished, lxd will be running and the baseline images will be downloaded.

### SSH setup

The build scripts require that there be a `sc2-lz` .ssh/config entry that works
without a password.  Part of the provision script involves creating this entry
and a `colosseum` key.  But to finalize the setup, you have to ensure that `ssh
sc2-lz` will reach the colosseum file proxy without a password.

If you already have a key that lets you access, colosseum, copy it to
`~/.ssh/colosseum` and make sure that `ssh sc2-lz` successfully runs without
needing a password.

If you can not reach sc2-lz without a password, you will need to manually add
your public key value to the `~/.ssh/authorized_keys` file on the landing zone
(file-proxy).

To do this, `ssh sc2-lz`, enter your password and then, from the file-proxy,
create an `authorized_keys` file:

```
mkdir ~/.ssh
chmod 700 ~/.ssh
touch ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys
```

Then edit ~/.ssh/authorized_keys and paste the value in `colosseum.pub` into
that file.  Once you do that, you will have password-free access to sc2-lz
which is a requirement for the build scripts.

## Build and run srs batch job

```
cd ansible
ansible-playbook srs.yml -e "ansible_python_interpreter=/usr/bin/python3"
```

To configure this playbook, you can change the `srslte_repo` and
`srslte_version` in `ansible/srs.yml` before running the playbook.
`srslte_version` can be a git sha or a git branch name.

You can also change the `batches` and/or `radio_args` parameters in that file
to configure the experiments that will be run.

## Build and run oai batch job

```
cd ansible
ansible-playbook oai.yml -e "ansible_python_interpreter=/usr/bin/python3"
```

To configure this playbook, you can change the `oai_repo` and
`oai_version` in `ansible/srs.yml` before running the playbook.
`oai_version` can be a git sha or a git branch name.

You can also change the `batches` and/or `radio_args` parameters in that file
to configure the experiments that will be run.

## Viewing Logs

Once the ansible output completes, there should be a pending batch job waiting
in the Colosseum experiments interface.  This job will take about 30 minutes to
run.  After it is finished, there will be logs in your Colosseum log directory.
To see them, ssh into sc2-lz and run `cd /share/nas/<team name>`.

## Acknowledgement

Thanks to Leonardo Bonati and Salvatore D'Oro for their contributions.
