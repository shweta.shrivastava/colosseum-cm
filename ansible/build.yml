# Ansible playbook for building image
- name: Delete image if it exists
  command: lxc image delete {{ hostname }}
  ignore_errors: yes

- name: Create container
  lxd_container:
    name: "{{ hostname }}"
    state: started
    source:
      type: image
      alias: "{{ base_image }}"
    profiles: ["default"]
    wait_for_ipv4_addresses: true
    timeout: 600

- name: Add Host
  add_host:
    name: "{{ hostname }}"
    ansible_connection: lxd

- name: Copy ssh keys
  delegate_to: "{{ hostname }}"
  copy:
    src: ~/.ssh/{{ ssh_key }}
    dest: /root/.ssh/id_rsa
    mode: 0600
  when: ssh_key is defined

- name: set authorized key
  delegate_to: "{{ hostname }}"
  authorized_key:
    user: root
    state: present
    key: "{{ lookup('file', '~/.ssh/{{ ssh_key }}.pub') }}"
  when: ssh_key is defined

- name: Get git sha for colosseum-cm
  command: git rev-parse HEAD
  register: gitresult

- name: Save colosseum-cm git sha to file
  delegate_to: "{{ hostname }}"
  lineinfile:
    path: "{{ sha_file }}"
    state: present
    create: yes
    line: "colosseum-cm: {{ gitresult.stdout }}"

- name: create /mnt/ramdisk
  delegate_to: "{{ hostname }}"
  file:
    path: /mnt/ramdisk
    state: directory
  become: yes

- name: Add entry to /etc/fstab to mount ramdisk
  delegate_to: "{{ hostname }}"
  lineinfile:
    path: /etc/fstab
    state: present
    create: yes
    line: "tmpfs /mnt/ramdisk tmpfs nodev,nosuid,noexec,nodiratime,size=20G 0 0"
  become: yes

- import_tasks: build-srs.yml
  when: srs is defined
- import_tasks: build-oai.yml
  when: oai is defined

- name: Copy radio_control
  delegate_to: "{{ hostname }}"
  copy:
    src: ../radio_control
    dest: "{{ root_dir }}"

- name: Build radio_control
  delegate_to: "{{ hostname }}"
  command: chdir={{ root_dir }}/radio_control pip3 install .
  become: yes

- name: Copy radio_api files
  delegate_to: "{{ hostname }}"
  copy:
    src: ../radio_api/{{ item }}
    dest: "{{ root_dir }}/radio_api"
    mode: 0755
  with_items:
  - start.sh
  - statistics.sh
  - status.sh
  - stop.sh
  - scenario_discontinuity.sh
  - update_environment.sh
  - update_outcomes.sh

- name: Copy init files
  delegate_to: "{{ hostname }}"
  copy:
    src: ../systemd/{{ item }}
    dest: /lib/systemd/system
    mode: 0644
  with_items:
  - radio.service
  become: yes

- name: Copy startup scripts
  delegate_to: "{{ hostname }}"
  copy:
    src: ../systemd/{{ item }}
    dest: /usr/local/bin
    mode: 0744
  with_items:
  - radio.sh
  become: yes

- name: enable services
  delegate_to: "{{ hostname }}"
  systemd:
    name: "{{ item }}"
    enabled: yes
    masked: no
  with_items:
  - radio
  become: yes

- name: Stop container
  lxd_container:
    name: "{{ hostname }}"
    state: stopped

- name: Publish container
  command: lxc publish {{ hostname }} --alias {{ hostname }}

- name: Export image
  command: lxc image export {{ hostname }} {{ images_dir }}/{{ hostname }}

- name: Delete the container
  lxd_container:
    name: "{{ hostname }}"
    state: absent
