- hosts: localhost
  vars:
    oai: True
    oai_repo: https://gitlab.eurecom.fr/oai/openairinterface5g.git
    oai_version: develop
    base_name: oai
    base_image: col-baseline-oai
    team_name: << team_name >>
    ssh_key: << ssh_key >>
    images_dir: /images
    root_dir: /root
    sha_file: "{{ root_dir }}/gitsha"
    config_auto_dir: ../config_auto
    config_template: ../config/template.conf.j2
    batch_template: ../batch/template.json.j2
    run_time: 120
    timestamp: "{{ curtime.stdout }}"
    conf_pre: oai
    run_index: "{{ 999 | random }}"
    freq: 1000e6
    nodes: "{{ range(1, 4)|list }}"

    radio_args:
      iperf: 100
      tcpdump: /logs/tun_srs.pcap
      #traffic:
      #  - {src: 2, dest: 1, rate_bps: .1e7}
      #  - {src: 3, dest: 1, rate_bps: .1e7}
      C: "{{ freq | int }}"
      usrp-args: addr=192.168.40.2

    teams:
      - "{{ {'team': 'team1'} | combine(radio_args) }}"

    batches:
      - {total_nodes: 3, team_nodes: 3, duration: 300, rf_scenario: 9991, team_start: 1}

  tasks:
    - name: Get a timestamp
      command: date +%Y%m%d%H%M%S
      register: curtime

    - name: Get this ansible branch number
      shell: "git branch | grep '*' | cut -d ' ' -f2 | tail -c4"
      register: ansible_branch

    - set_fact:
        hostname: "{{ base_name }}-{{ (oai_version | replace('_','-'))[-3:] }}-{{ ansible_branch.stdout }}"

    - name: Print some debug information
      debug:
        msg: "{{ images_dir }}/{{ hostname }}"
      tags: debug_info

    - import_tasks: build.yml
    - import_tasks: image-push.yml
    - import_tasks: conf-push.yml
