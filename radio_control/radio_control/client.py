import socket
import json
import time

def send_command(command, sockname='/tmp/radiod'):
    try:
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.settimeout(1.0)
        sock.connect(sockname)
        sock.sendall(command.encode())
        data = sock.recv(1024).decode()
        sock.close()
        return data
    except:
        return ''

def get_status(sockname='/tmp/radiod'):
    return send_command('status', sockname)

def start(sockname='/tmp/radiod'):
    return send_command('start', sockname)

def stop(sockname='/tmp/radiod'):
    return send_command('stop', sockname)

def boot(conf='', sockname='/tmp/radiod'):
    return send_command('boot ' + conf, sockname)

def publish_mo(mo='', sockname='/tmp/radiod'):
    return send_command('mo ' + mo, sockname)

def update_environment(env='', sockname='/tmp/radiod'):
    return send_command('env ' + env, sockname)

def wait_state(state='READY', timeout=None, interval=1.0, sockname='/tmp/radiod'):
    start_time = time.time()

    # Poll status until state is valid
    while (timeout is None) or ((time.time()-start_time) < timeout):
        try:
            status = json.loads(get_status(sockname))
            if status['STATUS'] == state.upper():
                return True
        except:
            # ignore errors here
            pass
        time.sleep(interval)

    # timed out
    return False
