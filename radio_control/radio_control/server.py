import time
import logging
import os
import json
import subprocess
import socketserver
import configparser
import socket
import re
import netifaces
import zmq
import pmt
import shlex
import shutil

RADIO_STARTFILE = '/tmp/radio_startfile'
SHUTDOWN_TIME = 120

class RadioControlHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    # Check if any processes are still running
    def is_active(self):
        for proc in self.server.procs:
            if proc.poll() is None:
                return True
        return False

    def run_post(self):
        if 'falcon' in self.server.conf:
            falcon_dir = os.path.dirname(self.server.conf['command'])
            logging.info("Looking for falcon log files in {}".format(falcon_dir))
            for filename in os.listdir(falcon_dir):
                _, file_ext = os.path.splitext(filename)
                full_filename = os.path.join(falcon_dir, filename)
                if (file_ext == '.csv' and os.path.getsize(full_filename) > 0) or\
                   (file_ext == '.log' and os.path.getsize(full_filename) > 1200):
                    shutil.copy2(full_filename, "/logs")
                    logging.info("Copied file: {} to /logs".format(full_filename))

        if 'post' in self.server.conf:
            if 'post_out' in self.server.conf:
                post_out = open(self.server.conf['post_out'], 'w')
                logging.info('Logging post command output to: {}'.format(self.server.conf['post_out']))
            else:
                post_out = None

            for command in self.server.conf['post']:
                self.server.procs.append(subprocess.Popen(shlex.split(command),
                                                          stdout=post_out, stderr=post_out))
                logging.info('Ran post command: {}, pid={}'.format(command, self.server.procs[-1].pid))

    # Boot the radio
    def boot(self, conf):
        logging.info('Config: {}'.format(conf))

        # Run pre-radio commands
        if 'pre' in conf:
            if 'pre_out' in conf:
                pre_out = open(conf['pre_out'], 'w')
                logging.info('Logging pre command output to: {}'.format(conf['pre_out']))
            else:
                pre_out = None
            for command in conf['pre']:
                self.server.procs.append(subprocess.Popen(shlex.split(command),
                                                          stdout=pre_out, stderr=pre_out))
                logging.info('Ran pre command: {}, pid={}'.format(command, self.server.procs[-1].pid))

        if 'command' in conf:
            tx_power = 0.0
            args = [conf['command']]
            if 'command_out' in conf:
                command_out = open(conf['command_out'], 'w')
                logging.info('Logging command output to: {}'.format(conf['command_out']))
            else:
                command_out = None

            # Use args from config file
            if 'args' in conf:
                if 'falcon' in conf:
                    falcon_dir = os.path.dirname(conf['command'])
                    settings_file = os.path.join(falcon_dir, "Settings-Loop-FalconEye.sh")
                    f = open(settings_file, "w")
                    for key, val in list(conf['args'].items()):
                        f.write('{}="{}"\n'.format(key, val))
                    f.close()
                    logging.info("Wrote falcon settings to file: {}".format(settings_file))
                    shutil.copy2(settings_file, "/logs")
                else:
                    for key, val in list(conf['args'].items()):
                        if len(key) == 1:
                            args.append('-' + key)
                        else:
                            args.append('--' + key)
                        args.append(str(val))
                        if key == 'tx-gain':
                            tx_power = eval(val)

            # Add startfile argument (and delete file if it exists)
            try:
                os.remove(RADIO_STARTFILE)
            except:
                pass
            if 'no_extra' not in conf:
                args.extend(['--startfile', RADIO_STARTFILE])

            # Set as master if col0 interface exists
            if 'col0' in netifaces.interfaces() and 'no_extra' not in conf:
                args.extend(['--master', '1'])

            # Parse colosseum config file
            colConfig = None
            colFreq = 1.0e9
            colBw = 20e6
            freq_adjust = 0.0
            if 'config' in conf:
                try:
                    colConfig = configparser.ConfigParser()
                    colConfig.read(conf['config'])
                    colFreq = eval(colConfig.get('RF','center_frequency'))
                    colBw = eval(colConfig.get('RF','rf_bandwidth'))
                except Exception as e:
                    logging.warning('Unable to parse colosseum config file: {}'.format(e))

            # Use node number if provided, otherwise get from the hostname
            node = None
            if 'node' in conf:
                node = conf['node']
            else:
                # Get radio node number
                r = re.search("[0-9]{1,3}$", socket.gethostname())
                if r is not None:
                     node = int(r.group(0).lstrip('0')) + 100

            # Start the radio if node number found
            if node is not None:
                if 'no_extra' not in conf:
                    args.extend(['--node', node])

                logging.info('Running radio radio with args: {}'.format(args))
                command_cwd = os.path.dirname(args[0])
                if command_cwd == '':
                    command_cwd = None
                self.server.procs.append(subprocess.Popen(args,
                                                          stdout=command_out, stderr=command_out,
                                                          cwd=command_cwd))
                self.server.state = 'BOOTING'
                self.server.info = 'Booting up radio'
                logging.info('Started radio process, pid={}'.format(self.server.procs[-1].pid))
                self.server.conf = conf
                self.server.mo_list = []
                self.server.env_list = []

                # Start Collaboration client if gateway
                if 'collab' in conf and ('col0' in netifaces.interfaces()) and ('monitor' not in conf):
                    logging.info('Found col0 interface, starting collaboration client')
                    args = [conf['collab']]
                    if colConfig is not None:
                        if colConfig.has_option('COLLABORATION', 'collab_server_ip'):
                            args.extend(['--server-ip', colConfig.get('COLLABORATION', 'collab_server_ip')])
                        if colConfig.has_option('COLLABORATION', 'collab_server_port'):
                            args.extend(['--server-port', colConfig.get('COLLABORATION', 'collab_server_port')])
                        if colConfig.has_option('COLLABORATION', 'collab_client_port'):
                            args.extend(['--client-port', colConfig.get('COLLABORATION', 'collab_client_port')])
                        if colConfig.has_option('COLLABORATION', 'collab_peer_port'):
                            args.extend(['--peer-port', colConfig.get('COLLABORATION', 'collab_peer_port')])

                        # use the address of col0 interface as client IP
                        if netifaces.AF_INET in netifaces.ifaddresses('col0'):
                            args.extend(['--client-ip', netifaces.ifaddresses('col0')[netifaces.AF_INET][0]['addr']])

                    if 'collab_log' in conf:
                        args.extend(['--log-config-filename', conf['collab_log']])

                    if 'rf.ul_freq' in conf['args']:
                        args.extend(['--ul-freq', conf['args']['rf.ul_freq']])

                    if 'rf.dl_freq' in conf['args']:
                        args.extend(['--dl-freq', conf['args']['rf.dl_freq']])

                    if 'collab_out' in conf:
                        collab_out = open(conf['collab_out'], 'w')
                        logging.info('Logging collab command output to: {}'.format(conf['collab_out']))
                    else:
                        collab_out = None

                    logging.info('Starting collaboration client with args: {}'.format(args))
                    self.server.procs.append(subprocess.Popen(args, stdout=collab_out, stderr=collab_out))
                    logging.info('Started collaboration client, pid={}'.format(self.server.procs[-1].pid))
                else:
                    logging.info('No collaboration interface found, not starting collaboration client')

                # Go ahead and start if started is set
                if 'started' in conf and (conf['started'] == 'true'):
                    f = open(RADIO_STARTFILE, 'w')
                    f.write('STARTED')
                    f.close()
                    logging.info('Wrote STARTED to: {}'.format(RADIO_STARTFILE))
            else:
                logging.warning('Unable to get node number from hostname: {}, radio not started'.format(
                    socket.gethostname()))
                self.server.state = 'ERROR'
                self.server.info = 'Unable to get node number from hostname, could not start radio'
        else:
            logging.warning('Config file does not have command for radio')


    # Socket handler
    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).decode().strip()
        logging.info('Got request: ' + self.data)

        lastState = self.server.state

        # Get current status
        if self.data.upper() == 'STATUS':

            # Make sure current status hasnt changed
            if self.server.state == 'BOOTING':
                # Check that radio has booted
                if not self.is_active() :
                    self.server.state = 'ERROR'
                    self.server.info = 'Radio stopped unexpectedly'
                else:
                    # See if radio has created the startfile
                    try:
                        f = open(RADIO_STARTFILE, 'r')
                        rstate = f.read()
                        if rstate == 'STARTED':
                            self.server.state = 'READY'
                            self.server.info = 'Radio is ready to start'
                        elif rstate == 'ERROR':
                            self.server.state = 'ERROR'
                            self.server.info = 'Error while booting radio'
                    except:
                        # this likely means the file hasn't been created yet, so no state change
                        pass

            elif self.server.state == 'ACTIVE':
                if not self.is_active():
                    self.server.state = 'ERROR'
                    self.server.info = 'Radio stopped unexpectedly'

            elif self.server.state == 'STOPPING':
                if not self.is_active():
                    self.server.state = 'FINISHED'
                    self.server.info = 'Radio finished'
                elif (time.time() - self.server.stop_time) > SHUTDOWN_TIME:
                    logging.info('Shutdown timed out, killing all processes')
                    # Kill all active procs
                    for proc in self.server.procs:
                        logging.info('Killing process: {}'.format(proc.pid))
                        try:
                            proc.kill()
                        except:
                            logging.warning('Caught exception trying to kill process')

                    self.server.procs = []
                    self.server.state = 'FINISHED'
                    self.server.info = 'Radio finished (shutdown timeout)'

        # Boot the radio
        elif self.data.upper().startswith('BOOT'):
            datasplit = self.data.split()
            if len(datasplit) > 1:
                cfilename = datasplit[1]
                logging.info('Booting radio with config file: ' + cfilename)

                # Get all parameters as json
                try:
                    with open(cfilename) as cfile:
                        self.boot(json.load(cfile))
                except Exception as e:
                    logging.warning('Unable to parse json config file: {} -- {}'.format(cfilename, e))
            else:
                logging.warning('No radio config file specified: unable to start radio')

        elif self.data.upper() == 'START':
            if self.server.state == 'READY':
                # set up zmq connections to radio
                self.server.context = zmq.Context()

                if 'mo-address' in self.server.conf:
                    logging.info('Opening mo socket to: {}'.format(self.server.conf['mo-address']))
                    self.server.mo_socket = self.server.context.socket(zmq.PUSH)
                    self.server.mo_socket.connect(self.server.conf['mo-address'])

                    # Send any saved MOs that came in before the radio started
                    for msg in self.server.mo_list:
                        try:
                            self.server.mo_socket.send(msg)
                            logging.info('Sent new MO to: {}'.format(self.server.conf['mo-address']))
                        except Exception as e:
                            logging.warning('Unable to send mandated outcomes to radio on port {} -- {}'.format(self.server.conf['mo-address'], e))

                if 'env-address' in self.server.conf:
                    logging.info('Opening env socket to: {}'.format(self.server.conf['env-address']))
                    self.server.env_socket = self.server.context.socket(zmq.PUSH)
                    self.server.env_socket.connect(self.server.conf['env-address'])

                    # Send any saved ENVs that came in before the radio started
                    for msg in self.server.env_list:
                        try:
                            self.server.env_socket.send(msg)
                            logging.info('Sent new ENV to: {}'.format(self.server.conf['env-address']))
                        except Exception as e:
                            logging.warning('Unable to send environment update to radio on port {} -- {}'.format(self.server.conf['env-address'], e))

                # Notify the radio
                if 'notify-address' in self.server.conf:
                    logging.info('Opening notify socket to: {}'.format(self.server.conf['notify-address']))
                    self.server.notify_socket = self.server.context.socket(zmq.PUSH)
                    self.server.notify_socket.connect(self.server.conf['notify-address'])
                    msg = zmq.Message(pmt.serialize_str(pmt.to_pmt('start')))
                    self.server.notify_socket.send(msg)

                # write startfile
                f = open(RADIO_STARTFILE, 'w')
                f.write('START')
                f.close()
                logging.info('Wrote START to: {}'.format(RADIO_STARTFILE))

                self.server.state = 'ACTIVE'
                self.server.info = 'Radio is active'
            else:
                self.server.state = 'ERROR'
                self.server.info = 'Attempted to start radio that was not ready'

        elif self.data.upper() == 'STOP':
            logging.info('Stopping radio')
            self.server.stop_time = time.time()

            if self.is_active():
                self.server.state = 'STOPPING'
                self.server.info = 'Stopping Radio'

                f = open(RADIO_STARTFILE, 'w')
                f.write('STOP')
                f.close()
                logging.info('Wrote STOP to: {}'.format(RADIO_STARTFILE))

                # Notify the radio
                if 'notify-address' in self.server.conf:
                    msg = zmq.Message(pmt.serialize_str(pmt.to_pmt('stop')))
                    self.server.notify_socket.send(msg)

                time.sleep(1.0) # give radio time to process message

                # Close sockets
                if 'notify-address' in self.server.conf:
                    logging.info('Closing notify socket')
                    self.server.notify_socket.close()

                if 'mo-address' in self.server.conf:
                    logging.info('Closing mo socket')
                    self.server.mo_socket.close()

                if 'env-address' in self.server.conf:
                    logging.info('Closing env socket')
                    self.server.env_socket.close()

                self.server.context.term()

                # Terminate all active procs
                for proc in self.server.procs:
                    logging.info('Terminating process: {}'.format(proc.pid))
                    try:
                        proc.terminate()
                    except:
                        logging.warning('Caught exception trying to terminate process')
            else:
                self.server.state = 'STOPPING'
                self.server.info = 'Attempted to stop non-active radio'

            # Run any post procs (these should terminate on their own)
            logging.info('Radio stopped')
            self.run_post()

        elif self.data.upper().startswith('MO'):
            logging.info('Received MO command')
            datasplit = self.data.split()
            if len(datasplit) > 1:
                mfilename = datasplit[1]

                # Get all parameters as json
                try:
                    with open(mfilename) as mfile:
                        mo = json.load(mfile)
                    logging.info('MO: {}'.format(mo))

                    # Convert to pmt and send to radio
                    if 'mo-address' in self.server.conf:
                        msg = zmq.Message(pmt.serialize_str(pmt.to_pmt(mo)))
                        try:
                            self.server.mo_socket.send(msg)
                            logging.info('Sent new MO to: {}'.format(self.server.conf['mo-address']))
                        except Exception as e:
                            logging.warning('Unable to send mandated outcomes to radio on port {}, saving for later -- {}'.format(self.server.conf['mo-address'], e))
                            self.server.mo_list.append(msg)

                except Exception as e:
                    logging.warning('Unable to parse mandated outcomes file: {} -- {}'.format(mfilename, e))

        elif self.data.upper().startswith('ENV'):
            logging.info('Received ENV command')
            datasplit = self.data.split()
            if len(datasplit) > 1:
                efilename = datasplit[1]

                # Get all parameters as json
                try:
                    with open(efilename) as efile:
                        env = json.load(efile)
                    logging.info('ENV: {}'.format(env))

                    # Convert to pmt and send to radio
                    if 'env-address' in self.server.conf:
                        msg = zmq.Message(pmt.serialize_str(pmt.to_pmt(env)))
                        try:
                            self.server.env_socket.send(msg)
                            logging.info('Sent new ENV to: {}'.format(self.server.conf['env-address']))
                        except Exception as e:
                            logging.warning('Unable to send environment update to radio on port {}, saving for later -- {}'.format(self.server.conf['env-address'], e))
                            self.server.env_list.append(msg)

                except Exception as e:
                    logging.warning('Unable to parse environment file: {} -- {}'.format(efilename, e))

        else:
            logging.info('Received unexpected command: {}'.format(self.data))

        # Log a message if the state changed
        if self.server.state != lastState:
            logging.info('Radio state changed {} -> {} ({})'.format(
                lastState, self.server.state, self.server.info))

        # send back json of current status as response
        status = {'STATUS': self.server.state,
                  'INFO': self.server.info}
        self.request.sendall(json.dumps(status, indent=4).encode())

def run_server(logfile='/logs/radiod.log', sockname='/tmp/radiod'):
    logging.basicConfig(format='%(asctime)s %(message)s',
                        filename=logfile,
                        level=logging.INFO)

    # Delete UNIX socket file if it exists
    try:
        os.remove(sockname)
    except:
        pass

    server = socketserver.UnixStreamServer(sockname, RadioControlHandler)
    logging.info('Started radiod server on socket: ' + sockname)
    server.state = 'OFF'
    server.info = 'Not started'
    server.procs = []

    server.serve_forever()
