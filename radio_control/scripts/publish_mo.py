#!/usr/bin/env python3
import sys
from radio_control.client import publish_mo

if len(sys.argv) > 1:
    mo = sys.argv[1]
else:
    mo = '/root/radio_api/mandated_outcomes.json'

publish_mo(mo)
