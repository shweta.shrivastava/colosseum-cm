#!/usr/bin/env python3
import argparse
import os
import struct
import binascii
import socket
import logging
import time
import json
import threading

HEADER_SIZE = 10

def parse_pkt(pkt):
    if len(pkt) < (HEADER_SIZE + 4):
        return None

    flow_id, count, src, dst = struct.unpack('IIBB', pkt[:HEADER_SIZE])
    msg = pkt[HEADER_SIZE:-4]
    pkt_crc, = struct.unpack('I', pkt[-4:])
    return flow_id, count, src, dst, msg, pkt_crc

class PacketGenerator:
    def __init__(self, pkt_size=1400, flow_id=0, src=0, dst=0):
        assert pkt_size >= (HEADER_SIZE + 4)
        self.pkt_size = pkt_size
        self.flow_id = flow_id
        self.src = src
        self.dst = dst
        self.count = 0
        self.pkt_crc = 0

    def get_packet(self):
        self.count += 1
        pkt = struct.pack('IIBB', self.flow_id, self.count, self.src, self.dst) +\
              os.urandom(self.pkt_size - HEADER_SIZE - 4)
        self.pkt_crc = binascii.crc32(pkt)
        pkt += struct.pack('I', self.pkt_crc)
        return pkt

class PacketSender(threading.Thread):
    def __init__(self, pkt_gen, interval=1.0, dest_ip='172.16.0.1', port=5000, start_time=0, end_time=float('inf')):
        threading.Thread.__init__(self)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
        self.pkt_gen = pkt_gen
        self.interval = interval
        self.dest_ip = dest_ip
        self.port = port
        self.running = True
        self.start_time = time.time() + start_time
        self.end_time = time.time() + end_time

    def send(self):
        pkt = self.pkt_gen.get_packet()
        self.sock.sendto(pkt, (self.dest_ip, self.port))
        logging.info("SEND,{},{},{},{},{},{},0".format(self.pkt_gen.flow_id, self.pkt_gen.src,
                                                       self.pkt_gen.dst, self.pkt_gen.count, len(pkt),
                                                       self.pkt_gen.pkt_crc))

    def run(self):
        while self.running and time.time() < self.end_time:
            if time.time() >= self.start_time:
                self.send()
                time.sleep(self.interval)

class PacketReceiver(threading.Thread):
    def __init__(self, dest_ip, port=5000, recv_size=4096):
        threading.Thread.__init__(self)
        self.recv_size = recv_size
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((dest_ip, port))
        self.running = True

    def recv(self):
        data, addr = self.sock.recvfrom(self.recv_size)
        return data

    def run(self):
        while self.running:
            pkt = self.recv()
            flow_id, count, src, dst, msg, pkt_crc = parse_pkt(pkt)
            logging.info("RECV,{},{},{},{},{},{},{}".format(
                flow_id, src, dst, count, len(pkt), pkt_crc, binascii.crc32(pkt[:-4])))

def traffic_gen(config_file, node=-1, flow=5000, subnet="172.16.0"):
    default_conf = {"rate_bps": 1e6, "start": 0, "end": float("inf"), "pkt_size": 1400}
    pkt_threads = []
    with open(config_file) as f:
        for line in f:
            try:
                tc = json.loads(line)
                print("Read traffic config from file: {}".format(tc), flush=True)
                if "src" in tc and "dest" in tc:
                    fc = default_conf.copy()
                    fc.update(tc)

                    # Update types in fc
                    for key, val in fc.items():
                        if key in ["src", "dest", "pkt_size", "flow"]:
                            fc[key] = int(val)
                        elif key in ["rate_bps", "start", "end"]:
                            fc[key] = float(val)

                    if "flow" not in fc:
                        fc["flow"] = flow

                    if fc["src"] == node:
                        interval = fc["pkt_size"] * 8 / fc["rate_bps"]
                        print("Starting packet generator with config: {}, interval: {}, subnet: {}".format(fc, interval, subnet), flush=True)

                        pg = PacketGenerator(fc["pkt_size"], fc["flow"], fc["src"], fc["dest"])
                        ps = PacketSender(pg, interval, "{}.{}".format(subnet, fc["dest"]), fc["flow"], fc["start"], fc["end"])
                        pkt_threads.append(ps)

                    if fc["dest"] == node:
                        print("Starting packet receiver with config: {}, subnet: {}".format(fc, subnet), flush=True)
                        pr = PacketReceiver("{}.{}".format(subnet, fc["dest"]), fc["flow"], fc["pkt_size"])
                        pkt_threads.append(pr)
            except Exception as e:
                print("Caught exception parsing {}: {}".format(line, e))

            flow += 1

    for pt in pkt_threads:
        pt.start()

    for pt in pkt_threads:
        pt.join()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--node", type=int, default=-1,
                        help="Node number for this node")
    parser.add_argument("-o", "--logfile", default="traffic.log",
                        help="Logfile to log traffic generated and received")
    parser.add_argument("-i", "--config-file", default=None,
                        help="Traffic configuration file")
    parser.add_argument("-s", "--subnet", default="172.16.0",
                        help="Traffic subnet")
    parser.add_argument("-f", "--flow-start", type=int, default=5000,
                        help="Flow number to start with")
    parser.add_argument("-t", "--team", default=None,
                        help="team name")
    args = parser.parse_args()

    if args.logfile:
        logging.basicConfig(filename=args.logfile, level=logging.INFO,
                            format="%(asctime)s.%(msecs)03d,{},%(message)s".format(args.team),
                            datefmt="%Y-%m-%d %H:%M:%S")

    print("Starting traffic generator, args: {}".format(args), flush=True)

    if args.config_file:
        traffic_gen(args.config_file, args.node, args.flow_start, args.subnet)

if __name__ == '__main__':
    main()
