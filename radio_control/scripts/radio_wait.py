#!/usr/bin/env python3
import sys
from radio_control.client import wait_state

# First argument is state
if len(sys.argv) > 1:
    state = sys.argv[1]
else:
    state = 'READY'

# Second argument is timeout
if len(sys.argv) > 2:
    timeout = eval(sys.argv[2])
else:
    timeout = None

# Third argument is the poll interval
if len(sys.argv) > 3:
    interval = eval(sys.argv[3])
else:
    interval = 1.0

# Return code is 1 if timeout
if not wait_state(state, timeout, interval):
    print('Timed out waiting for: {}'.format(state))
    sys.exit(1)
