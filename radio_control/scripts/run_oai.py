#!/usr/bin/env python3
import argparse
import time
import subprocess
import os
import netifaces
import json
import shutil

import logging
logging.basicConfig(format='[%(asctime)s.%(msecs)03d %(levelname)s] %(message)s', datefmt='%Y-%m-%d,%H:%M:%S',
                    level=logging.INFO)

def get_arguments(cmd):
    args = []
    logging.info('Command: %s', cmd)
    cmd_out = subprocess.run([cmd, "--help"], stdout=subprocess.PIPE).stdout
    for argline in str(cmd_out).split():
        argwords = argline.split()
        if len(argwords) > 0 and argwords[0].startswith('-'):
            args.append(argwords[0].split(':')[0])
    logging.info("Valid args: %s", args)
    return args

def oai_start(cmd, oai_dir, config_file=None, extra_args=[], logfile=None):
    oai_out = None
    if logfile:
        try:
            oai_out = open(logfile, "w")
        except:
            logging.warning("Unable to open file for logging {}: {}".format(cmd, logfile))

    full_cmd = os.path.join(oai_dir, cmd)
    valid_args = get_arguments(full_cmd)
    oai_args = [full_cmd]
    if config_file is not None:
        oai_args.extend(['-O', config_file])
    argidx = 0
    while argidx < len(extra_args):
        if extra_args[argidx] in valid_args:
            oai_args.append(extra_args[argidx])
            if argidx < (len(extra_args) - 1) and not extra_args[argidx+1].startswith('-'):
                logging.info("Adding %s=%s to arguments", extra_args[argidx], extra_args[argidx+1])
                argidx += 1
                oai_args.append(extra_args[argidx])
            else:
                logging.info("Adding %s to arguments", extra_args[argidx])
        else:
            logging.info("Ignoring argument {} for {}".format(extra_args[argidx], cmd))
        argidx += 1

    logging.info("Running with args: {}".format(oai_args))

    proc = subprocess.Popen(oai_args, stdout=oai_out, stderr=oai_out,
                            cwd=oai_dir)
    logging.info("{} started, config file: {}, pid: {}".format(cmd, config_file, proc.pid))

    return proc

def run_oai(args, extra_args=[]):
    logging.info("Starting OAI, args: {}, extra: {}".format(args, extra_args))

    if args.startfile:
        f = open(args.startfile, 'w')
        f.write('STARTED')
        f.close()
        logging.info('Wrote STARTED to: {}'.format(args.startfile))

        logging.info("Waiting for radio to start, monitoring: {}".format(args.startfile))
        while True:
            try:
                f = open(args.startfile, "r")
                if f.read() == "START":
                    break
            except:
                pass
            time.sleep(0.1)

    logging.info("Starting radio")

    procs = []
    completed_procs = []

    # Always run with noS1 enabled
    extra_args.extend(['-E', '--noS1', '--nokrnmod', '1', '--phy-test'])

    # Run gNodeB if master
    if args.master:
        procs.append(oai_start("nr-softmodem", args.oai_dir, os.path.join(args.config_dir, args.gnb_config), extra_args, args.gnb_log))

        # Wait for network interface to start
        while "oaitun_enb1" not in netifaces.interfaces():
            time.sleep(0.5)

        logging.info("oaitun_enb1 interface created: %s", netifaces.ifaddresses('oaitun_enb1'))

        if args.tcpdump:
            try:
                tcpdump_out = open("/logs/tcpdump.log", "w")
            except:
                tcpdump_out = None
                logging.warning("Unable to open file for logging tcpdump: /logs/tcpdump.log")
            procs.append(subprocess.Popen(["tcpdump", "-i", "oaitun_enb1", "-w", args.tcpdump],
                                          stdout=tcpdump_out, stderr=tcpdump_out))
            logging.info("tcpdump started, logging to: {}, pid: {}".format(args.tcpdump, procs[-1].pid))

        if args.iperf:
            time.sleep(3)
            try:
                iperf_out = open("/logs/iperf.log", "w")
            except:
                iperf_out = None
                logging.warning("Unable to open file for logging iperf: /logs/iperf.log")
            procs.append(subprocess.Popen(["iperf", "-s"], stdout=iperf_out, stderr=iperf_out))
            logging.info("iperf server started, pid: {}".format(procs[-1].pid))

    else:
        # Give some time for enb to start
        time.sleep(10)

        # Copy config files to run dir
        shutil.copy2(os.path.join(args.config_dir, "rbconfig.raw"), args.oai_dir)
        shutil.copy2(os.path.join(args.config_dir, "reconfig.raw"), args.oai_dir)

        procs.append(oai_start("nr-uesoftmodem", args.oai_dir, None, extra_args, args.ue_log))

        # Wait for network interface to start
        while "oaitun_ue1" not in netifaces.interfaces():
            time.sleep(0.5)

        logging.info("oaitun_ue1 interface created: %s", netifaces.ifaddresses('oaitun_ue1'))

        if args.tcpdump:
            try:
                tcpdump_out = open("/logs/tcpdump.log", "w")
            except:
                tcpdump_out = None
                logging.warning("Unable to open file for logging tcpdump: /logs/tcpdump.log")
            procs.append(subprocess.Popen(["tcpdump", "-i", "oairun_ue1", "-w", args.tcpdump],
                                          stdout=tcpdump_out, stderr=tcpdump_out))
            logging.info("tcpdump started, logging to: {}, pid: {}".format(args.tcpdump, procs[-1].pid))
            time.sleep(1)

        subprocess.call(["ping", "10.0.1.1", "-c", "10"])

        if args.iperf:
            try:
                iperf_out = open("/logs/iperf.log", "w")
            except:
                iperf_out = None
                logging.warning("Unable to open file for logging iperf: /logs/iperf.log")
            procs.append(subprocess.Popen(["iperf", "-c", "10.0.1.1", "-t", str(args.iperf)],
                                          stdout=iperf_out, stderr=iperf_out))
            logging.info("iperf client started, running for {} seconds, pid: {}".format(args.iperf, procs[-1].pid))

    if args.traffic:
        logging.info("Traffic configuration: {}".format(args.traffic))
        conf_file = open("/logs/traffic.conf", "w")
        for tc in eval(args.traffic):
            conf_file.write("{}\n".format(json.dumps(tc)))
        conf_file.close()

        try:
            traf_out = open("/logs/traffic_gen.log", "w")
        except:
            traf_out = None
            logging.warning("Unable to open file for logging traffic: /logs/traffic_gen.log")
        procs.append(subprocess.Popen(["traffic_gen.py", "-i", "/logs/traffic.conf",
                                       "-o", "/logs/traffic.log", "-n", str(args.node), "-t", str(args.team)],
                                      stdout=traf_out, stderr=traf_out))
        logging.info("Traffic generator started, pid: {}".format(procs[-1].pid))

    logging.info("Radio started")

    if args.startfile:
        f = open(args.startfile, 'w')
        f.write('STARTED')
        f.close()
        logging.info('Wrote STARTED to: {}'.format(args.startfile))

    # Wait for stop
    while True:
        try:
            f = open(args.startfile, "r")
            if f.read() == "STOP":
                break
        except:
            pass

        for proc in procs:
            if proc.pid not in completed_procs:
                retcode = proc.poll()
                if retcode is not None:
                    logging.info("Process with pid: {} has exited, retcode: {}".format(proc.pid, retcode))
                    completed_procs.append(proc.pid)
        time.sleep(0.1)

    logging.info("Stopping radio")
    for proc in procs:
        if proc.pid not in completed_procs:
            logging.info('Terminating process: {}'.format(proc.pid))
            try:
                proc.terminate()
            except:
                logging.warning('Caught exception trying to terminate process {}'.format(proc.pid))

    if args.startfile:
        f = open(args.startfile, 'w')
        f.write('STOPPED')
        f.close()

    logging.info("Radio stopped")

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--master", default=0, type=int,
                        help="Run this node as the master (eNB)")
    parser.add_argument("--iperf", default=0, type=int,
                        help="Run iperf for specified number of seconds")
    parser.add_argument("--traffic", default=None,
                        help="traffic generator configuration")
    parser.add_argument("--oai-dir", default="/root/openairinterface5g/cmake_targets/ran_build/build",
                        help="OAI installation directory")
    parser.add_argument("--config-dir", default="/root/oai_config",
                        help="gnb configuration file")
    parser.add_argument("--gnb-config", default="gnb.conf",
                        help="gnb configuration file")
    parser.add_argument("--gnb-log", default="/logs/gnb.log",
                        help="file to log enb stdout to")
    parser.add_argument("--ue-log", default="/logs/ue.log",
                        help="file to log ue stdout to")
    parser.add_argument("--startfile", default=None,
                        help="file to monitor for radio start")
    parser.add_argument("--node", type=int, default=-1,
                        help="Node number for this node")
    parser.add_argument("--tcpdump", default=None,
                        help="File to dump tun device pcap to")
    parser.add_argument("--team", default=None,
                        help="team name")
    args, extra_args = parser.parse_known_args()
    run_oai(args, extra_args)

if __name__ == "__main__":
    main()
